package com.bortamas.thermo.core.usecase

import com.bortamas.thermo.core.repository.TemperatureRepository
import com.bortamas.thermo.core.util.ProgramState

class SetSelectTemperatureUseCase(private val repository: TemperatureRepository) {
    operator fun invoke(state: ProgramState, temperature: Float) {
        repository.setSelectedTemperature(state, temperature)
    }
}
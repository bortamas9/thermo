package com.bortamas.thermo.core

import com.bortamas.thermo.core.remotesource.TemperatureRemoteSource
import com.bortamas.thermo.core.repository.TemperatureRepository
import com.bortamas.thermo.core.usecase.GetCurrentTemperatureUseCase
import com.bortamas.thermo.core.usecase.GetSelectedTemperatureUseCase
import com.bortamas.thermo.core.usecase.SetSelectTemperatureUseCase
import org.koin.dsl.module

private fun createUseCasesModule() = module {
    factory { GetCurrentTemperatureUseCase(repository = get()) }
    factory { GetSelectedTemperatureUseCase(repository = get()) }
    factory { SetSelectTemperatureUseCase(repository = get()) }
}

private fun createRepositoryModule() = module {
    single { TemperatureRepository(remoteSource = get()) }
}

private fun createRemoteSourceModule() = module {
    factory { TemperatureRemoteSource() }
}

fun createCoreModules() = listOf(
    createUseCasesModule(),
    createRepositoryModule(),
    createRemoteSourceModule()
)
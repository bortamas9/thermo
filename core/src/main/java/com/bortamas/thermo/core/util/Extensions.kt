package com.bortamas.thermo.core.util

import com.bortamas.thermo.core.model.SelectedTemperaturesModel

fun SelectedTemperaturesModel.getTemperatureByState(state: ProgramState) = when (state) {
    ProgramState.Day -> daySelectedTemperature
    ProgramState.Night -> nightSelectedTemperature
    ProgramState.Eco -> ecoSelectedTemperature
}

fun SelectedTemperaturesModel.setTemperature(state: ProgramState, temperature: Float) {
    when (state) {
        ProgramState.Day -> daySelectedTemperature = temperature
        ProgramState.Night -> nightSelectedTemperature = temperature
        ProgramState.Eco -> ecoSelectedTemperature = temperature
    }
}
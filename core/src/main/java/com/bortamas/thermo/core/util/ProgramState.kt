package com.bortamas.thermo.core.util

enum class ProgramState {
    Day,
    Night,
    Eco
}
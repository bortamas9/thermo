package com.bortamas.thermo.core.model

data class SelectedTemperaturesModel(
    var daySelectedTemperature: Float,
    var nightSelectedTemperature: Float,
    var ecoSelectedTemperature: Float
)
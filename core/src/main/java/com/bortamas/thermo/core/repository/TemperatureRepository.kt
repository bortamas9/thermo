package com.bortamas.thermo.core.repository

import com.bortamas.thermo.core.model.SelectedTemperaturesModel
import com.bortamas.thermo.core.remotesource.TemperatureRemoteSource
import com.bortamas.thermo.core.util.ProgramState
import com.bortamas.thermo.core.util.getTemperatureByState
import com.bortamas.thermo.core.util.setTemperature
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow

@OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
class TemperatureRepository(private val remoteSource: TemperatureRemoteSource) {

    private val currentTemperatureChannel = ConflatedBroadcastChannel<Float>()
    val currentTemperature = currentTemperatureChannel.asFlow()

    private val selectedTemperatureChannel = ConflatedBroadcastChannel<Float>()
    val selectedTemperature = selectedTemperatureChannel.asFlow()

    private var selectedTemperaturesModel: SelectedTemperaturesModel? = null

    fun getCurrentTemperature() {
        currentTemperatureChannel.offer(remoteSource.getCurrentTemperature())
    }

    fun getSelectedTemperature(state: ProgramState) {
        val selectedTemperature =
            selectedTemperaturesModel?.getTemperatureByState(state) ?: remoteSource.getSelectedTemperatures().also {
                selectedTemperaturesModel = it
            }.getTemperatureByState(state)
        selectedTemperatureChannel.offer(selectedTemperature)
    }

    fun setSelectedTemperature(state: ProgramState, temperature: Float) {
        selectedTemperaturesModel?.setTemperature(state, temperature)
        getSelectedTemperature(state)
    }
}
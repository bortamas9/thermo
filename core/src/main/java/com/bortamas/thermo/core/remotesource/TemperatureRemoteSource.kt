package com.bortamas.thermo.core.remotesource

import com.bortamas.thermo.core.model.SelectedTemperaturesModel

class TemperatureRemoteSource {

    fun getCurrentTemperature() = 20.5F

    fun getSelectedTemperatures() = SelectedTemperaturesModel(21.5f, 19f, 13f)
}
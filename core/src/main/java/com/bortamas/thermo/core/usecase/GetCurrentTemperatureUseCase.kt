package com.bortamas.thermo.core.usecase

import com.bortamas.thermo.core.repository.TemperatureRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
class GetCurrentTemperatureUseCase(private val repository: TemperatureRepository) {

    operator fun invoke() = repository.currentTemperature.also { repository.getCurrentTemperature() }
}
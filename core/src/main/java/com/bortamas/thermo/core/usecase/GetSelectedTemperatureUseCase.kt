package com.bortamas.thermo.core.usecase

import com.bortamas.thermo.core.repository.TemperatureRepository
import com.bortamas.thermo.core.util.ProgramState

class GetSelectedTemperatureUseCase(private val repository: TemperatureRepository) {
    operator fun invoke(state: ProgramState) = repository.selectedTemperature.also {
        repository.getSelectedTemperature(state)
    }
}
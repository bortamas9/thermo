package com.bortamas.thermo.feature.changetemperature.viewholder

import com.bortamas.thermo.R
import com.bortamas.thermo.TemperatureItemBinding
import com.bortamas.thermo.feature.changetemperature.uimodel.TemperatureUiModel
import com.halcyonmobile.typedrecyclerviewadapter.ClickableTypedAdapterFactory
import com.halcyonmobile.typedrecyclerviewadapter.RecyclerItem

class TemperatureItemFactory(onClickListener: (position: Int) -> Unit) : ClickableTypedAdapterFactory<TemperatureUiModel, TemperatureItemBinding>(onClickListener) {
    override fun bind(model: TemperatureUiModel, holder: BindingViewHolder<TemperatureItemBinding>, position: Int, payloads: List<Any>) {
        holder.binding.uiModel = model
    }

    override fun canHandle(item: RecyclerItem) = item is TemperatureUiModel

    override fun getLayoutId() = R.layout.item_temperature
}
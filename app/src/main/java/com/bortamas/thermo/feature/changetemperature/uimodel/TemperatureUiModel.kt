package com.bortamas.thermo.feature.changetemperature.uimodel

import com.halcyonmobile.typedrecyclerviewadapter.RecyclerItem

data class TemperatureUiModel(override val id: String, val temperature: Float) : RecyclerItem
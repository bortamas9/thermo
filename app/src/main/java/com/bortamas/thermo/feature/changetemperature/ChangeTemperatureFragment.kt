package com.bortamas.thermo.feature.changetemperature

import android.os.Bundle
import android.view.View
import androidx.core.view.updatePadding
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bortamas.thermo.ChangeTemperatureBinding
import com.bortamas.thermo.R
import com.bortamas.thermo.core.util.ProgramState
import com.bortamas.thermo.feature.changetemperature.viewholder.TemperatureItemFactory
import com.bortamas.thermo.shared.ThermoFragment
import com.halcyonmobile.typedrecyclerviewadapter.RecyclerAdapter

class ChangeTemperatureFragment : ThermoFragment<ChangeTemperatureBinding, ChangeTemperatureViewModel>(R.layout.fragment_change_temperature) {

    private val args by navArgs<ChangeTemperatureFragmentArgs>()
    override val viewModel: ChangeTemperatureViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val homeAdapter = RecyclerAdapter().apply {
            addCellFactories(TemperatureItemFactory {
                with(requireBinding().listView) {
                    if (currentItem == it) {
                        viewModel.selectTemperature(it, ProgramState.values()[args.programState])
                        findNavController().navigateUp()
                    } else {
                        currentItem = it
                    }
                }
            })
            submitList(viewModel.temperatureList)
        }

        with(requireBinding()) {
            setupPager(listView, homeAdapter)
            listView.currentItem = viewModel.getPositionByTemperature(args.selectedTemperature)
        }

    }

    private fun setupPager(pager: ViewPager2, avatarAdapter: RecyclerAdapter) {
        pager.apply {
            (getChildAt(0) as? RecyclerView)?.let { recycler ->
                val pagerPadding = getHeightInPixels() / 2 - context.resources.getDimensionPixelSize(R.dimen.pager_padding)
                recycler.clipToPadding = false
                recycler.updatePadding(top = pagerPadding, bottom = pagerPadding)
                recycler.overScrollMode = ViewPager2.OVER_SCROLL_NEVER
            }

            adapter = avatarAdapter
            offscreenPageLimit = AVATAR_PAGE_COUNT
            setPageTransformer { page, position ->
                val absPosition = kotlin.math.abs(position)
                val scaleFactor = MIN_SCALE.coerceAtLeast(VALUE_ONE - absPosition / 3)
                page.scaleY = scaleFactor
                page.scaleX = scaleFactor
                page.alpha = MIN_ALPHA.coerceAtLeast(VALUE_ONE - absPosition / 3)
            }
        }
    }

    private fun getHeightInPixels(): Int {
        val displayMetrics = resources.displayMetrics
        return displayMetrics.heightPixels
    }

    companion object {
        private const val MIN_SCALE = 0.35F
        private const val MIN_ALPHA = 0F
        private const val VALUE_ONE = 1

        @field:ViewPager2.OffscreenPageLimit
        private const val AVATAR_PAGE_COUNT = 3
    }

}
package com.bortamas.thermo.feature.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.bortamas.thermo.ProgramStateSwitcherBinding
import com.bortamas.thermo.R
import com.bortamas.thermo.core.util.ProgramState

class ProgramStateSwitcherView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding = ProgramStateSwitcherBinding.inflate(LayoutInflater.from(context), this, true)
    private var programStateClickListener: ((ProgramState) -> Unit)? = null

    init {
        with(binding) {
            dayButton.setOnClickListener {
                setSelectedState(ProgramState.Day)
                programStateClickListener?.invoke(ProgramState.Day)
            }
            nightButton.setOnClickListener {
                setSelectedState(ProgramState.Night)
                programStateClickListener?.invoke(ProgramState.Night)
            }
            ecoButton.setOnClickListener {
                setSelectedState(ProgramState.Eco)
                programStateClickListener?.invoke(ProgramState.Eco)
            }
        }
    }

    fun setSelectedState(programState: ProgramState) {
        with(binding.dayButton) {
            val isDay = programState == ProgramState.Day
            isSelected = isDay
            elevation = resources.getDimension(if (isDay) R.dimen.switcher_elevation_selected else R.dimen.switcher_elevation)
        }
        with(binding.nightButton) {
            val isNight = programState == ProgramState.Night
            isSelected = isNight
            elevation = resources.getDimension(if (isNight) R.dimen.switcher_elevation_selected else R.dimen.switcher_elevation)
        }
        with(binding.ecoButton) {
            val isEco = programState == ProgramState.Eco
            isSelected = isEco
            elevation = resources.getDimension(if (isEco) R.dimen.switcher_elevation_selected else R.dimen.switcher_elevation)
        }
    }

    fun setProgramStateClickListener(listener: (ProgramState) -> Unit) {
        programStateClickListener = listener
    }
}
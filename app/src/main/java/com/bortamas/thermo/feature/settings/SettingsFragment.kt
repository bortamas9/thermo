package com.bortamas.thermo.feature.settings

import com.bortamas.thermo.R
import com.bortamas.thermo.SettingsBinding
import com.bortamas.thermo.shared.ThermoFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class SettingsFragment : ThermoFragment<SettingsBinding, SettingsViewModel>(R.layout.fragment_settings) {

    override val viewModel: SettingsViewModel by viewModel()

}
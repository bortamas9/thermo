package com.bortamas.thermo.feature.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.bortamas.thermo.SharedPreferenceManager
import com.bortamas.thermo.core.usecase.GetCurrentTemperatureUseCase
import com.bortamas.thermo.core.usecase.GetSelectedTemperatureUseCase
import com.bortamas.thermo.core.usecase.SetSelectTemperatureUseCase
import com.bortamas.thermo.core.util.ProgramState
import com.bortamas.thermo.util.Event

class HomeViewModel(
    getCurrentTemperature: GetCurrentTemperatureUseCase,
    private val getSelectedTemperature: GetSelectedTemperatureUseCase,
    private val sharedPreferencesManager: SharedPreferenceManager
) : ViewModel() {

    val currentTemperature: LiveData<Float> = getCurrentTemperature().asLiveData()
    val selectedTemperature: LiveData<Float> = getSelectedTemperature(sharedPreferencesManager.programState).asLiveData()

    val selectedProgramState: ProgramState
        get() = sharedPreferencesManager.programState

    private val _event = MutableLiveData<Event<Action>>()
    val event: LiveData<Event<Action>> = _event

    fun programStateClicked(programState: ProgramState) {
        sharedPreferencesManager.programState = programState
        getSelectedTemperature(programState)
    }

    fun openTemperatureSelection() {
        _event.value = Event(Action.OpenChangeTemperature(selectedTemperature.value!!, selectedProgramState))
    }

    sealed class Action {
        class OpenChangeTemperature(val temperature: Float, val programState: ProgramState) : Action()
    }
}
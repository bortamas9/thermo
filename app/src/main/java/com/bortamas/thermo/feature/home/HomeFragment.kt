package com.bortamas.thermo.feature.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bortamas.thermo.HomeBinding
import com.bortamas.thermo.R
import com.bortamas.thermo.shared.ThermoFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : ThermoFragment<HomeBinding, HomeViewModel>(R.layout.fragment_home) {

    override val viewModel: HomeViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(requireBinding()) {
            programStateSwitcher.setProgramStateClickListener {
                this@HomeFragment.viewModel.programStateClicked(it)
            }
        }

        viewModel.event.observe(viewLifecycleOwner, Observer {
            when(val action = it.consume()) {
                is HomeViewModel.Action.OpenChangeTemperature ->{
                    findNavController().navigate(HomeFragmentDirections.homeFragmentToChangeTemperatureFragment(action.temperature, action.programState.ordinal))
                }
            }
        })
    }
}
package com.bortamas.thermo.feature.util

import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter

@BindingAdapter("isVisible")
fun View.setIsVisible(visible: Boolean) {
    isVisible = visible
}
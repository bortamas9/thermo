package com.bortamas.thermo.feature

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.bortamas.thermo.R
import com.bortamas.thermo.ThermoBinding

class ThermoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ThermoBinding>(this, R.layout.activity_thermo)

        val navController = findNavController(R.id.main_host_fragment)
        binding.bottomNavigation.setupWithNavController(navController)
    }
}
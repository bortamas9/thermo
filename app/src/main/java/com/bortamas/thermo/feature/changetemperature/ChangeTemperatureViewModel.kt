package com.bortamas.thermo.feature.changetemperature

import androidx.lifecycle.ViewModel
import com.bortamas.thermo.core.usecase.GetSelectedTemperatureUseCase
import com.bortamas.thermo.core.usecase.SetSelectTemperatureUseCase
import com.bortamas.thermo.core.util.MAX_TEMPERATURE
import com.bortamas.thermo.core.util.MIN_TEMPERATURE
import com.bortamas.thermo.core.util.ProgramState
import com.bortamas.thermo.feature.changetemperature.uimodel.TemperatureUiModel

class ChangeTemperatureViewModel(private val setSelectTemperatureUseCase: SetSelectTemperatureUseCase) : ViewModel() {

    val temperatureList = (MIN_TEMPERATURE * 2..MAX_TEMPERATURE * 2).map {
        TemperatureUiModel(it.toString(), it / 2f)
    }

    fun getPositionByTemperature(temperature: Float) = temperatureList.indexOfFirst { it.temperature == temperature }

    fun selectTemperature(temperaturePosition: Int, programState: ProgramState) {
        setSelectTemperatureUseCase(programState, temperatureList[temperaturePosition].temperature)
    }
}
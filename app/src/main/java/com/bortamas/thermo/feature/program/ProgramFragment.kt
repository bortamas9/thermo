package com.bortamas.thermo.feature.program

import com.bortamas.thermo.ProgramBinding
import com.bortamas.thermo.R
import com.bortamas.thermo.shared.ThermoFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProgramFragment : ThermoFragment<ProgramBinding, ProgramViewModel>(R.layout.fragment_program) {

    override val viewModel: ProgramViewModel by viewModel()

}
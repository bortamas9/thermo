package com.bortamas.thermo.feature.view

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.bortamas.thermo.R
import com.bortamas.thermo.TemperatureBinding

class TemperatureView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding = TemperatureBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        context.obtainStyledAttributes(attrs, R.styleable.TemperatureView).apply {
            getResourceId(R.styleable.TemperatureView_textAppearance, -1).let {
                binding.temperature.setTextAppearance(it)
                binding.degreeSign.setTextAppearance(it)
            }
        }.recycle()
        binding.degreeSign.text = SpannableString(resources.getString(R.string.degree_celcius)).apply {
            setSpan(RelativeSizeSpan(0.3f), 0, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }

    fun setTemperature(temperature: Float) {
        binding.temperature.text = SpannableString(temperature.toString()).apply {
            setSpan(RelativeSizeSpan(0.5f), length - 1, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
}
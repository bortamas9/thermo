package com.bortamas.thermo

import com.bortamas.thermo.core.createCoreModules
import com.bortamas.thermo.feature.changetemperature.ChangeTemperatureViewModel
import com.bortamas.thermo.feature.home.HomeViewModel
import com.bortamas.thermo.feature.program.ProgramViewModel
import com.bortamas.thermo.feature.settings.SettingsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {
    single { SharedPreferenceManager(context = get()) }
}

val homeModule = module {
    viewModel {
        HomeViewModel(
            getCurrentTemperature = get(),
            getSelectedTemperature = get(),
            sharedPreferencesManager = get()
        )
    }
}

val changeTemperatureModule = module {
    viewModel {
        ChangeTemperatureViewModel(setSelectTemperatureUseCase = get())
    }
}

val programModule = module {
    viewModel {
        ProgramViewModel()
    }
}

val settingsModule = module {
    viewModel {
        SettingsViewModel()
    }
}

fun createModules() = listOf(
    appModule,
    homeModule,
    changeTemperatureModule,
    programModule,
    settingsModule
).plus(createCoreModules())
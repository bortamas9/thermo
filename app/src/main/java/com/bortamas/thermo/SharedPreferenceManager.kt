package com.bortamas.thermo

import android.app.Application
import android.content.Context
import com.bortamas.thermo.core.util.ProgramState
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


class SharedPreferenceManager(context: Application) {

    val sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE)

    var programState by PreferenceFieldDelegate.ProgramStateEnum(PROGRAM_STATE)

    companion object {
        private const val PREFERENCES_KEY = "preferences"
        private const val PROGRAM_STATE = "programState"
    }
}

private sealed class PreferenceFieldDelegate<T>(protected val key: String, protected val defaultValue: T) : ReadWriteProperty<SharedPreferenceManager, T> {

    class ProgramStateEnum(key: String, defaultValue: Enum<ProgramState> = ProgramState.Day) : PreferenceFieldDelegate<Enum<ProgramState>>(key, defaultValue) {
        override fun getValue(thisRef: SharedPreferenceManager, property: KProperty<*>) =
            ProgramState.values()[thisRef.sharedPreferences.getInt(key, 0)]

        override fun setValue(thisRef: SharedPreferenceManager, property: KProperty<*>, value: Enum<ProgramState>) =
            thisRef.sharedPreferences.edit().putInt(key, value.ordinal).apply()
    }

}